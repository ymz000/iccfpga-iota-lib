#ifndef TRANSFERS_H
#define TRANSFERS_H

#include <stdint.h>

#include "bundle.h"
#include "transaction.h"


namespace Transfers {
	Transaction* prepareTransfers(BundleCTX* bundle_ctx, uint8_t security, Transaction* txs, int lenTXs, int maxTXs, bool fixMBug = true, uint32_t timestamp = 0);
	Transaction* signTransactions(BundleCTX* bundle_ctx, Transaction* first, const uint8_t* seedBytes, uint8_t security);
	Transaction* reverseTransactions(Transaction* first);
}

#endif //TRANSFERS_H
