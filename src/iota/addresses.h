#ifndef ADDRESSES_H
#define ADDRESSES_H

#include "iota_types.h"
#include "keccak/sha3.h"

namespace Address {
/** @brief Computes the public address.
 *  @parm seed_bytes the seed as big-endian 48-byte integer
 *  @idx index of the address
 *  @security securtiy level
 *  @address_bytes address as a big-endian 48-byte integer
 */
void getPublicAddr(const unsigned char *seed_bytes, uint32_t idx,
                     unsigned int security, unsigned char *address_bytes);

/** @brief Computes the full address string in base-27 encoding.
 *  The full address consists of the actual address (81 chars) plus 9 chars of
 *  checksum.
 *  @param address_bytes the address, i.e. hash, in binarary representation
 *  @full_address target for the full address as a string
 */
void getAddressWithChecksum(const unsigned char *address_bytes,
                            char *full_address);

void getAddressTrytes(const unsigned char *seed_bytes, uint32_t idx,
						unsigned int security, char *address);

}
#endif // ADDRESSES_H
